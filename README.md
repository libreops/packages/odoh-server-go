# odoh-server-go

Oblivious DoH Server written in go

[gh:Source code](https://github.com/cloudflare/odoh-server-go)

## ietf draft

[Oblivious DNS Over HTTPS](https://tools.ietf.org/html/draft-pauly-dprive-oblivious-doh-04)

## Download

Download odoh-server-go binary (latest version)

- [odoh-server-go](https://gitlab.com/libreops/packages/odoh-server-go/-/jobs/artifacts/main/raw/odoh-server-go?job=run-build)

## Debian Package

There is also a debian package (just the binary and the systemd service file) but it works!

- [odoh-server-go_amd64.deb](https://gitlab.com/libreops/packages/odoh-server-go/-/jobs/artifacts/main/browse?job=run-deb-build)

## systemd

Copy odoh service file to systemd system directory:

```bash
cp odoh-server-go.service /etc/systemd/system/odoh-server-go.service
```

and then:

```bash
systemctl enable odoh-server-go.service
systemctl  start odoh-server-go.service
```
